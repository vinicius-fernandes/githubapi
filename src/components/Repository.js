import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import ModalDetails from './ModalDetails';
import ApiServices from '../constants/ApiServices';
import ModalLoading from './ModalLoading';
import Colors from '../constants/Colors';

export default class Repository extends React.Component {

    state = {
        owner: '',
        isModalDetailsVisible: false,  
        isLoading: false,
    };

    showDetails = async () => {

        this.setState({isLoading: true});

        var login = this.props.val.owner.login;
        const URI = ApiServices.getOwner + login;

        try {
            let response = await fetch(URI);

            if (response.ok) {
                let responseJson = await response.json();

                this.setState({
                    isLoading: false,
                    owner: responseJson,
                    isModalDetailsVisible: true,
                });
            } else {
                this.setState({ isLoading: false});
                alert('Não foi possível encontrar os dados. Verifique e tente novamente.');
            }
        } catch (error) {
            this.setState({isLoading: false});
            alert('Falha ao carregar dados. Tente novamente.');
        }
    }

    render() { 

        return (
            <TouchableOpacity onPress={this.showDetails}>

                <View key={this.props.keyval} style={styles.container}>

                    <View style={styles.headerTop}>
                        <Image
                            style={styles.avatar}
                            source={{uri: this.props.val.owner.avatar_url}} />

                        <Text style={styles.headerTitle}>{this.props.val.name}</Text>
                    </View>

                    <Text style={styles.avatarText}>{this.props.val.owner.login}</Text>

                    <View style={styles.dividerHorizontal}></View>

                    <View>
                        <Text style={{fontFamily: 'Roboto'}}><Text style={{fontWeight: 'bold'}}>Descrição: </Text>{this.props.val.description}</Text>
                    </View>
                    
                    <TouchableOpacity onPress={this.props.onDelete} style={styles.btnDelete}>
                        <Text style={styles.btnDeleteText}>x</Text>
                    </TouchableOpacity>

                </View>

                <ModalLoading onClose={ ()=> this.setState({isLoading: false})} modalVisible={this.state.isLoading} />

                <ModalDetails
                    modalVisible={this.state.isModalDetailsVisible}
                    onClose={ ()=> this.setState({isModalDetailsVisible: false})}
                    owner={this.state.owner}
                    repository={this.props.val}  />

            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.textColor,
        position: 'relative',
        padding: 15,
        justifyContent: 'space-between',
        paddingRight: 70,
        borderBottomWidth: 3,
        borderBottomColor: '#a09d99',
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 0,
        marginTop: 8,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 0,
        borderRightWidth: 3,
        borderRightColor: '#a09d99',
    },
    headerTop: {
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    headerTitle: {
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        marginLeft: 20,
        fontSize: 14,
        alignSelf: 'flex-start',
        color: Colors.textColorDefault,
    },
    avatar: {
        borderWidth: 2,
        borderColor: Colors.backgroundColor,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
        padding: 30,
    },
    avatarText: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'Roboto',
        fontSize: 12,
        paddingTop: 5,
        color: Colors.textColorDefault,
    },
    dividerHorizontal: {
        height: 1,
        backgroundColor: Colors.dividerColor,
        marginTop: 15,
        marginBottom: 10,
    },
    btnDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.backgroundColor,
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10,
        borderBottomWidth: 2,
        borderBottomColor: '#e6e6e6',
        borderRightWidth: 2,
        borderRightColor: '#e6e6e6',
    },
    btnDeleteText: {
        color: Colors.textColor,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: 16,
    },
});
