import React from 'react';
import { StyleSheet, View, Text, Modal, TextInput, TouchableOpacity } from 'react-native'
import Colors from '../constants/Colors';

export default class ModalAddNote extends React.Component {

    state = {
        owner: '',
        repo: '',
    };

    onAddRepository = () => {

        if (this.state.owner.trim() && this.state.repo.trim()) {

            this.props.onAdd(this.state.owner, this.state.repo);
            this.setState({
                owner: '',
                repo: '',
            });
        } else
            alert('Informe o nome do Owner e o nome do Repositório que deseja adicionar.')
    };

    render(){

        return(
            <Modal visible={this.props.modalVisible} animationTYpe="fade" transparent={true} onRequestClose={ this.props.onClose }>
                <View style={styles.container}>

                    <View style={styles.header}>
                        <Text style={styles.modalTitle}>ADICIONAR REPOSITÓRIO</Text>
                    </View>

                    <View style={styles.innerContainer}>

                            <TextInput 
                                style={styles.textInput}
                                autoCorrect={true}
                                blurOnSubmit={true}
                                underlineColorAndroid='transparent'
                                value={this.state.owner}
                                minHeight={50}
                                maxHeight={200}
                                keyboardType = 'ascii-capable'
                                autoFocus
                                onChangeText={(owner) => this.setState({owner})} 
                                placeholder = 'Id do Owner...'
                                placeholderTextColor='#333'
                                underlineColor='transparent' />

                            <TextInput 
                                style={styles.textInput}
                                autoCorrect={true}
                                blurOnSubmit={true}
                                underlineColorAndroid='transparent'
                                value={this.state.repo}
                                minHeight={50}
                                maxHeight={200}
                                keyboardType = 'ascii-capable'
                                onChangeText={(repo) => this.setState({repo})} 
                                placeholder = 'Id do Repositório...'
                                placeholderTextColor='#333'
                                underlineColor='transparent' />

                            <View style={styles.btnContainer}>
                                <TouchableOpacity onPress={this.onAddRepository} style={[styles.btn, styles.btnSuccess]}>
                                    <Text style={styles.btnText}>Adicionar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={ this.props.onClose } style={[styles.btn, styles.btnDefault]}>
                                    <Text style={styles.btnTexDefault}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>

                    </View>

                </View>
            </Modal>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        padding: 20,
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 75,
        marginBottom: -15,
    },
    modalTitle: {
        margin: 10,
        marginLeft: 0,
        textAlign: 'left',
        color: Colors.textColor,
        fontSize: 14,
        padding: 15,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        width: 250,
    },
    innerContainer: {
        padding: 10,
        borderRadius: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textInput: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        padding: 10,
        marginTop: 20,
        marginRight: 10,
        marginLeft: 10,
        fontFamily: 'Roboto',
    },
    btnContainer: {
        marginTop: 30,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopColor: Colors.dividerColor,
        borderTopWidth: 1,
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        borderBottomWidth: 3,
        borderBottomColor: Colors.btnBorderColor,
        borderRightWidth: 3,
        borderRightColor: '#c8c5c0',
        margin: 10,
        padding: 16,
    },
    btnText: {
        color: Colors.textColor,
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 14,
    },
    btnTexDefault: {
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 14,
        color: Colors.textColorDefault,
    },
    btnSuccess: {
        backgroundColor: Colors.btnSuccess,
    },
    btnDefault: {
        backgroundColor: Colors.btnDefault,
    },
});
